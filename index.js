const Discord = require('discord.js');

const client = new Discord.Client();
const config = require('./config.json');

client.on('ready', () => {
  console.log(`${client.user.tag} is ready in ${client.guilds.size} servers with ${client.users.size} users!`);
});

client.on('message', async message => {
  if (message.content.startsWith('djs ping')) {
    const msg = await message.channel.send('🏓 Pong!');
      
    msg.edit(`🏓 Pong! \`${msg.createdTimestamp - message.createdTimestamp}ms\``);
      
  }
});

client.login(config.token);